﻿/*
 *Create an enumerable collection for Task 10 which is Search Name problem.
 * The Person class was exist then two new classes (People and PeopleEnum) is made.
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace Task16__IEnumerableUpgrade
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Person[] peopleArray = new Person[5]
                   {
            new Person("Azi", "Smith","123345", "Stavanger", "Azi@com",32),
            new Person("Sara", "Johnson","19999345", "Bergen", "Sara@com",40),
            new Person("Martin", "Jones","13488885", "Oslo", "Martin@com",29),
            new Person("Marta", "KArimi","45454545", "Stavanger", "Marta@com",33),
            new Person("Margarit", "Jackson","78787843", "Paris", "Margarit@com",45),
                   };
            People peopleList = new People(peopleArray);
            foreach (Person p in peopleList)
              
                 Console.WriteLine(p.FirstName + " " + p.LastName);

            IEnumerable<string> namesOfPeople = 
                from Person person in peopleList
                where person.GetFullName().Length <= 9 // Here searching in person array and check 
                                                       // which full name length is less than 9.
                select person.GetFullName();
            foreach (var name in namesOfPeople)
            {
                Console.WriteLine("********");
                Console.WriteLine($"lenght of {name} is less than 9");
            }

        }

    }

    }
