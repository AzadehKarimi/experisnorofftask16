﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task16__IEnumerableUpgrade
{
    class People : IEnumerable
    {
        private Person[] _people;// make array od people in Person type
        public People(Person[] pArray)// Fill up the People collocation.
        {
            _people = new Person[pArray.Length];

            for (int i = 0; i < pArray.Length; i++)
            {
                _people[i] = pArray[i];
            }
        }
        // Implementation for the GetEnumerator method.
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public PeopleEnum GetEnumerator()
        {
            return new PeopleEnum(_people);
        }
    }
}

