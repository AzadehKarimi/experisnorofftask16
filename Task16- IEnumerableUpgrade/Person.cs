﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Task16__IEnumerableUpgrade
{
    class Person
    {
        private string firstName;
        private string lastName;
        private string telephone;
        private string address;
        private string email;
        private int age;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Telephone { get => telephone; set => telephone = value; }
        public string Address { get => address; set => address = value; }
        public string Email { get => email; set => email = value; }
        public int Age { get => age; set => age = value; }

        //Constructor
        public Person()
        {

        }
        //Constructor
        public Person(string firstName, string lastName, string telephone, string address, string email,int age)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Telephone = telephone;
            this.Address = address;
            this.Email = email;
            this.Age = age;

        }
        public string GetFullName()
        {
            return $"{FirstName } { LastName}";
        }


    }


}